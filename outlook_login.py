import os
import time

from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

chrome_driver = r"chromedriver_win32\chromedriver.exe"
os.environ['webdriver.chrome.driver'] = chrome_driver
options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-logging'])

driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
driver.get('https://outlook.live.com/owa/')
driver.implicitly_wait(20)


sign_in_button = driver.find_element_by_link_text('Войти')
sign_in_button.click()
driver.implicitly_wait(20)

user_mail = 'Dmytro.Yarosh@cs.khpi.edu.ua'
user_pass = 'Fenix24042001'


email_input_id = "i0116"
email_input = driver.find_element_by_id(email_input_id)
email_input.send_keys(user_mail)

email_input.send_keys(Keys.ENTER)


time.sleep(10)
password_input_id = 'i0118'
password_input = driver.find_element_by_xpath("//*[@id='i0118']")

password_input.send_keys(user_pass)

password_input.send_keys(Keys.ENTER)

ignored_exceptions = (NoSuchElementException, StaleElementReferenceException,)
sign_in_button_two = WebDriverWait(driver, 15, ignored_exceptions=ignored_exceptions) \
    .until(expected_conditions.presence_of_element_located((By.ID, 'idSIButton9')))
sign_in_button_two.click()

time.sleep(20)
folder = driver.find_elements_by_name('test_folder')
folder.click()
driver.implicitly_wait(20)

time.sleep(20)
folder = driver.find_elements_by_class_name('ms-Button-label label-161')
folder.click()

print("Complete")
