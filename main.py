from openpyxl import load_workbook, Workbook
import smtplib


def create_excel():
    wb_ad = load_workbook(filename='AD.xlsx')
    wb_t1 = load_workbook(filename='Training A.xlsx')
    wb_t2 = load_workbook(filename='Training B.xlsx')
    wb_t3 = load_workbook(filename='Training C.xlsx')
    wb = Workbook()
    ws = wb.active

    for ad_column, cell in enumerate(wb_ad['Sheet1']['C'], 1):
        general_flag = False
        for column, cell_t1 in enumerate(wb_t1['Sheet1']['C'], 1):
            result = list()

            if cell.value == cell_t1.value:
                general_flag = True
                result.extend([wb_t1['Sheet1'][f'A{column}'].value, wb_t1['Sheet1'][f'B{column}'].value,
                               wb_t1['Sheet1'][f'D{column}'].value])

                for column, cell_t2 in enumerate(wb_t2['Sheet1']['C'], 1):
                    if cell.value == cell_t2.value:
                        result.extend([wb_t2['Sheet1'][f'D{column}'].value])
                        break
                    if cell_t2.value == wb_t2['Sheet1']['C'][-1].value:
                        result.extend([''])

                for column, cell_t3 in enumerate(wb_t3['Sheet1']['C'], 1):
                    if cell.value == cell_t3.value:
                        result.extend([wb_t3['Sheet1'][f'D{column}'].value])
                        break
                    if cell_t3.value == wb_t3['Sheet1']['C'][-1].value:
                        result.extend([''])
                result.extend([wb_t1['Sheet1'][f'E{column}'].value, cell_t1.value])

                ws.append(result)

        if general_flag:
            continue

        for column, cell_t2 in enumerate(wb_t2['Sheet1']['C'], 1):

            result = list()
            if cell.value == cell_t2.value:
                general_flag = True
                result.extend([wb_t2['Sheet1'][f'A{column}'].value, wb_t2['Sheet1'][f'B{column}'].value,
                               '', wb_t2['Sheet1'][f'D{column}'].value, '',
                               wb_t2['Sheet1'][f'E{column}'].value, cell_t2.value])

                for column, cell_t3 in enumerate(wb_t3['Sheet1']['C'], 1):
                    if cell.value == cell_t3.value:
                        result.extend([wb_t3['Sheet1'][f'E{column}'].value, cell_t3.value])

                ws.append(result)

        if general_flag:
            continue

        for column, cell_t3 in enumerate(wb_t3['Sheet1']['C'], 1):
            if cell.value == cell_t3.value:
                result = [wb_t3['Sheet1'][f'A{column}'].value, wb_t3['Sheet1'][f'B{column}'].value,
                          wb_t3['Sheet1'][f'D{column}'].value, '', '',
                          wb_t3['Sheet1'][f'E{column}'].value, cell_t3.value]
                ws.append(result)

        ws.append([wb_ad['Sheet1'][f'A{ad_column}'].value, wb_ad['Sheet1'][f'B{ad_column}'].value,
                   wb_ad['Sheet1'][f'D{ad_column}'].value, '', '', '', cell.value])
    filename = 'test.xlsx'
    wb.save(filename=filename)

    return filename


def search_train(filename):
    wb = load_workbook(filename=filename)

    for column, cell in enumerate(wb['Sheet']['C'], 1):
        if not cell.value:
            send_mail(mail=wb['Sheet'][f'G{column}'], name=wb['Sheet'][f'A{column}'], training=wb['Sheet'][f'C1'], )

    for column, cell in enumerate(wb['Sheet']['D'], 1):
        if not cell:
            send_mail(mail=wb['Sheet'][f'G{column}'], name=wb['Sheet'][f'A{column}'], training=wb['Sheet'][f'D1'], )

    for column, cell in enumerate(wb['Sheet']['E'], 1):
        if not cell:
            send_mail(mail=wb['Sheet'][f'G{column}'], name=wb['Sheet'][f'A{column}'], training=wb['Sheet'][f'E1'], )

    return


def send_mail(mail, name, training):
    server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    server.login("test@gmail.com", "password")
    server.sendmail('test@gmail.com', mail.value,
                    f'DEAR {name.value} YOU NEED TO FINISH {training.value}')
    server.quit()

    return


if __name__ == '__main__':
    file = create_excel()
    search_train(filename=file)
